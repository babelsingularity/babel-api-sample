﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BabelWebApi.Models;
//using AmoryTechLibrary;

namespace BabelWebApi.Controllers
{
    public class PublicationController : ApiController
    {
        // GET api/publication
        public List<Publication> Get()
        {
            return Publication.GetPublications();
        }

        // GET api/publication/5
        public Publication Get(int id)
        {
            //Sample ID: 10002
            return new Publication(id);
        }

        //// POST api/publication
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/publication/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/publication/5
        //public void Delete(int id)
        //{
        //}
    }
}
