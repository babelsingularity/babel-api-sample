﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
//using AmoryTechLibrary;

namespace BabelWebApi.DAL
{
    public class PublicationDataAccess
    {
        private static string ConnectionString = Config.ConnectionString;

        public static PublicationData GetPublicationById(Int32 iPublicationId)
        {
            var rPublication = new PublicationData();

            DataTable dtPubData = new DataTable();
            try
            {
                SqlConnection Conn = new SqlConnection(ConnectionString);
                SqlCommand myCommand = new SqlCommand("GetBabelPublicationById", Conn);
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.Parameters.AddWithValue("@PublicationId", iPublicationId);
                myCommand.Connection.Open();
                SqlDataAdapter daPub = new SqlDataAdapter(myCommand);
                daPub.Fill(dtPubData);
                myCommand.Connection.Close();

                if (dtPubData.Rows.Count > 0)
                {
                    rPublication.PublicationId = iPublicationId;
                    rPublication.CategoryId = (Int32)DbFunctions.CheckDbNull(dtPubData.Rows[0]["CategoryId"]);
                    rPublication.CategoryName = DbFunctions.NullString(dtPubData.Rows[0]["CategoryName"]);
                    rPublication.ParentCategoryId = (Int32)DbFunctions.CheckDbNull(dtPubData.Rows[0]["ParentCategoryId"]);
                    rPublication.ParentCategoryName = DbFunctions.NullString(dtPubData.Rows[0]["ParentCategoryName"]);
                    rPublication.AuthorId = (Int32)DbFunctions.CheckDbNull(dtPubData.Rows[0]["AuthorId"]);
                    rPublication.AuthorName = DbFunctions.NullString(dtPubData.Rows[0]["AuthorName"]);
                    rPublication.IsDisplayAuthor = (Boolean)DbFunctions.CheckDbNull(dtPubData.Rows[0]["IsDisplayAuthor"]);
                    rPublication.IsListedAuthor = (Boolean)DbFunctions.CheckDbNull(dtPubData.Rows[0]["IsListedAuthor"]);
                    rPublication.IsActive = (Boolean)DbFunctions.CheckDbNull(dtPubData.Rows[0]["IsActive"]);
                    rPublication.IsFeatured = (Boolean)DbFunctions.CheckDbNull(dtPubData.Rows[0]["IsFeatured"]);
                    rPublication.ImageFileName = DbFunctions.NullString(dtPubData.Rows[0]["ImageFileName"]);
                    rPublication.ImageFileNameSmall = DbFunctions.NullString(dtPubData.Rows[0]["ImageFileNameSmall"]);
                    rPublication.ImageFileNameSmall = rPublication.ImageFileNameSmall.Equals(String.Empty) ? rPublication.ImageFileName : rPublication.ImageFileNameSmall;
                    rPublication.PostImageFile = DbFunctions.NullString(dtPubData.Rows[0]["PostImageFile"]);
                    rPublication.PostDate = (DateTime)DbFunctions.CheckDbNull(dtPubData.Rows[0]["PostDate"]);
                    rPublication.PostUpdateDate = DbFunctions.CheckDbNull(dtPubData.Rows[0]["PostUpdateDate"]) == null ? (DateTime)DbFunctions.CheckDbNull(dtPubData.Rows[0]["PostUpdateDate"]) : rPublication.PostDate;
                    rPublication.Title = DbFunctions.NullString(dtPubData.Rows[0]["Title"]);
                    rPublication.Preview = DbFunctions.NullString(dtPubData.Rows[0]["Preview"]);
                    rPublication.KeyWords = DbFunctions.NullString(dtPubData.Rows[0]["KeyWords"]);
                    rPublication.HeaderHtml = DbFunctions.NullString(dtPubData.Rows[0]["HeaderHtml"]);
                    rPublication.Content = DbFunctions.NullString(dtPubData.Rows[0]["Content"]);
                }
            }
            catch (Exception ex)
            {
                //ErrorTracking.LogRequestErrorInfo(ex);
                rPublication = new PublicationData { PublicationId = -1 };
            }

            return rPublication;
        }

        public static List<PublicationData> GetPublications()
        {
            List<PublicationData> PubList = new List<PublicationData>();

            DataTable dtPubData = new DataTable();
            try
            {
                SqlConnection Conn = new SqlConnection(ConnectionString);
                SqlCommand myCommand = new SqlCommand("GetBabelPublications", Conn);
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.Connection.Open();
                SqlDataAdapter daPub = new SqlDataAdapter(myCommand);
                daPub.Fill(dtPubData);
                myCommand.Connection.Close();

                foreach (DataRow rPub in dtPubData.Rows)
                {
                    var rPublication = new PublicationData();

                    rPublication.PublicationId = (Int32)DbFunctions.CheckDbNull(dtPubData.Rows[0]["PublicationId"]);
                    rPublication.IsActive = true;
                    rPublication.CategoryId = (Int32)DbFunctions.CheckDbNull(dtPubData.Rows[0]["CategoryId"]);
                    rPublication.CategoryName = DbFunctions.NullString(dtPubData.Rows[0]["CategoryName"]);
                    rPublication.ImageFileName = DbFunctions.NullString(dtPubData.Rows[0]["ImageFileName"]);
                    rPublication.PostDate = (DateTime)DbFunctions.CheckDbNull(dtPubData.Rows[0]["PostDate"]);
                    rPublication.PostUpdateDate = DbFunctions.CheckDbNull(dtPubData.Rows[0]["PostUpdateDate"]) == null ? (DateTime)DbFunctions.CheckDbNull(dtPubData.Rows[0]["PostUpdateDate"]) : rPublication.PostDate;
                    rPublication.Title = DbFunctions.NullString(dtPubData.Rows[0]["Title"]);
                    rPublication.Preview = DbFunctions.NullString(dtPubData.Rows[0]["Preview"]);
                    rPublication.KeyWords = DbFunctions.NullString(dtPubData.Rows[0]["KeyWords"]);
                }
            }
            catch (Exception ex)
            {
                //ErrorTracking.LogRequestErrorInfo(ex);
            }

            return PubList;
        }
    }

    public struct PublicationData
    {
        public int PublicationId;
        public bool IsActive;
        public bool IsFeatured;
        public int CategoryId;
        public string CategoryName;
        public int ParentCategoryId;
        public string ParentCategoryName;
        public DateTime PostDate;
        public DateTime PostUpdateDate;
        public string Title;
        public string Preview;
        public int AuthorId;
        public string AuthorName;
        public string PostImageFile;
        public string ImageFileName;
        public string ImageFileNameSmall;
        public string Content;
        public string HeaderHtml;
        public string KeyWords;
        public bool IsDisplayAuthor;
        public bool IsListedAuthor;
    }
}