﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BabelWebApi.DAL
{
    public class DbFunctions
    {
        public static object CheckDbNull(Object oIn)
        {
            if (oIn.Equals(DBNull.Value))
            {
                return null;
            }
            else
            {
                return oIn;
            }
        }

        public static string NullString(Object oArg)
        {
            if (oArg.Equals(DBNull.Value))
            {
                return string.Empty;
            }
            else
            {
                return oArg.ToString();
            }
        }
    }
}