﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BabelWebApi.DAL;
//using AmoryTechLibrary;

namespace BabelWebApi.Models
{
    public class Publication
    {
        
        public int PublicationId;
        public bool IsActive;
        public bool IsFeatured;
        public int CategoryId;
        public string CategoryName;
        public int ParentCategoryId;
        public string ParentCategoryName;
        public DateTime PostDate;
        public DateTime PostUpdateDate;
        public string Title;
        public string Preview;
        public int AuthorId;
        public string AuthorName;
        public string PostImageFile;
        public string ImageFileName;
        public string ImageFileNameSmall;
        public string Content;
        public string HeaderHtml;
        public string KeyWords;
        public bool IsDisplayAuthor;
        public bool IsListedAuthor;

        public Publication()
        {
            PublicationId = -1;
        }

        public Publication(int iPubId)
        {
            try
            {
                PublicationData pubData = PublicationDataAccess.GetPublicationById(iPubId);

                PublicationId = pubData.PublicationId;
                IsActive = pubData.IsActive;
                IsFeatured = pubData.IsFeatured;
                CategoryId = pubData.CategoryId;
                CategoryName = pubData.CategoryName;
                ParentCategoryId = pubData.ParentCategoryId;
                ParentCategoryName = pubData.ParentCategoryName;
                PostDate = pubData.PostDate;
                PostUpdateDate = pubData.PostUpdateDate;
                Title = pubData.Title;
                Preview = pubData.Preview;
                AuthorId = pubData.AuthorId;
                AuthorName = pubData.AuthorName;
                PostImageFile = pubData.PostImageFile;
                ImageFileName = pubData.ImageFileName;
                ImageFileNameSmall = pubData.ImageFileNameSmall;
                Content = pubData.Content;
                HeaderHtml = pubData.HeaderHtml;
                KeyWords = pubData.KeyWords;
                IsDisplayAuthor = pubData.IsDisplayAuthor;
                IsListedAuthor = pubData.IsListedAuthor;
            }
            catch (Exception ex)
            {
                //ErrorTracking.LogRequestErrorInfo(ex, true);
                PublicationId = -1;
            }
        }

        public static List<Publication> GetPublications()
        {
            List<Publication> rList = new List<Publication>(); 
            
            List<PublicationData> pDataList = PublicationDataAccess.GetPublications();

            foreach (PublicationData pubData in pDataList)
            {
                var p = new Publication
                {
                    PublicationId = pubData.PublicationId,
                    IsActive = pubData.IsActive,
                    CategoryId = pubData.CategoryId,
                    CategoryName = pubData.CategoryName,
                    PostDate = pubData.PostDate,
                    PostUpdateDate = pubData.PostUpdateDate,
                    Title = pubData.Title,
                    Preview = pubData.Preview,
                    ImageFileName = pubData.ImageFileName,
                    KeyWords = pubData.KeyWords,
                };

                rList.Add(p);
            }


            return rList;
        }
        
    }
}